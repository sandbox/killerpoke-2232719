<?php

/**
 * Result of FeedsSocialmediaFetcher::fetch().
 */
class FeedsSocialmediaFetcherResult extends FeedsHTTPFetcherResult {

  protected $auth;
  protected $service;

  /**
   * Constructor.
   */
  public function __construct($url = NULL, $auth = NULL, $service = NULL) {
    $this->auth = $auth;
    $this->service = $service;

    parent::__construct($url);
  }

  /**
   * Return a drupal_http_request object for the service.
   */
  private function service_requests() {
    $requests = array();
    $requests['twitter'] = drupal_http_request($this->url, array(
      'method' => 'GET',
      'headers' => array('Authorization' => $this->auth, )
    ));

    $requests['facebook'] = drupal_http_request(
      $this->url . '?access_token=' . $this->auth,
      array('method' => 'GET', )
    );

    $requests['youtube'] = drupal_http_request(
      $this->url,
      array('method' => 'GET', )
    );

    if(in_array($this->service, array_keys($requests))) {
      return $requests[$this->service];
    } else {
      return drupal_http_request($this->url, array('method' => 'GET', ));
    }

  }

  /**
   * Overrides FeedsFetcherResult::getRaw();
   */
  public function getRaw() {
    feeds_include_library('http_request.inc', 'http_request');

    $result = $this->service_requests();

    if (!in_array($result->code, array(200, 201, 202, 203, 204, 205, 206))) {
      throw new Exception(t('Download of @url failed with code !code.', array('@url' => $this->url, '!code' => $result->code)));
    }

    return $this->sanitizeRaw($result->data);
  }
}

/**
 * Fetches social media data via HTTP.
 */
class FeedsSocialmediaFetcher extends FeedsHTTPFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $config = $source->importer->getConfig();
    $importer_config = $config['fetcher']['config'];

    $fetcher_result = new FeedsSocialmediaFetcherResult($this->service_url(
      $source_config['service'],
      $source_config['name']
    ), $importer_config['auth_' . $source_config['service']],
    $source_config['service']);
    return $fetcher_result;
  }

  /**
   * Clear caches.
   */
  public function clear(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    $url = $this->service_url($source_config['service'], $source_config['name']);
    feeds_include_library('http_request.inc', 'http_request');
    http_request_clear_cache($url);
  }

  /**
   * Implements FeedsFetcher::request().
   */
  public function request($feed_nid = 0) {
    feeds_dbg($_GET);
    @feeds_dbg(file_get_contents('php://input'));

    try {
      feeds_source($this->id, $feed_nid)->existing()->import();
    }
    catch (Exception $e) {
      // In case of an error, respond with a 503 Service (temporary) unavailable.
      header('HTTP/1.1 503 "Not Found"', NULL, 503);
      drupal_exit();
    }

    // Will generate the default 200 response.
    header('HTTP/1.1 200 "OK"', NULL, 200);
    drupal_exit();
  }


  /**
   * Definition of all avaiable services.
   */
  private function available_services() {
    return array(
      'twitter' => 'Twitter',
      'facebook' => 'Facebook',
      'youtube' => 'YouTube',
    );
  }

  /**
   * Define service urls. [NAME] will be replaced by @param $name.
   */
  private function service_url($service, $name) {
    $service_urls = array(
      'twitter' => 'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=[NAME]',
      'facebook' => 'https://graph.facebook.com/[NAME]/posts',
      'youtube' => 'https://gdata.youtube.com/feeds/api/videos?q=[NAME]&alt=json'
    );

    if(in_array($service, array_keys($this->available_services()))) {
      return str_replace('[NAME]', $name, $service_urls[$service]);
    } else {
      return false;
    }
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {

    $d = array();

    foreach ($this->available_services() as $key => $name) {
      $d['auth_' . $key] = '';
    }

    return $d;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = array();
      foreach($this->available_services() as $key => $name) {
        $form['auth_' . $key] = array(
        '#type' => 'textfield',
        '#title' => t($name . ' Authorization Token'),
        '#description' => t('Enter your ' . $name . ' Authorization token, you got from '
          . 'requesting a oauth-token token.'),
       '#default_value' => $this->config['auth_' . $key],
     );
    }

    return $form;
  }

  /**
   * Expose source form.
   */
  public function sourceForm($source_config) {
    $form = array();

    $form['service'] = array(
      '#type' => 'select',
      '#title' => t('Service'),
      '#description' => t('Select one of the avaiable Services'),
      '#default_value' => isset($source_config['service']) ? $source_config['service'] : 'twitter',
      '#options' => $this->available_services(),
      '#required' => TRUE,
    );

    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('Enter the profile or site name.'),
      '#default_value' => isset($source_config['name']) ? $source_config['name'] : '',
      '#maxlength' => NULL,
      '#required' => TRUE,
    );

    return $form;
  }

  /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {

    if(!in_array($values['service'], array_keys($this->available_services()))) {
      $form_key = 'feeds][' . get_class($this) . '][service';
      form_set_error($form_key, t('Unknown Service.', array('%service' => $values['service'])));
    }
  }

  public function sourceSave(FeedsSource $source) { }
  public function sourceDelete(FeedsSource $source) { }
  public function subscribe(FeedsSource $source) { }
  public function unsubscribe(FeedsSource $source) { }
}
