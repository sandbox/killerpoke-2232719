<?php

/**
 * Parser Implementation for Socialmedia data.
 */
class FeedsSocialmediaParser extends FeedsParser {

  /**
   * Implementation of FeedsParser::parse().
   */
  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {

    $mappings = $source->importer->processor->config['mappings'];
    $result = new FeedsParserResult();

    if(isset($source->config[FeedsSocialmediaFetcher])) {
      $config = $source->config[FeedsSocialmediaFetcher];

      $raw = json_decode(trim($fetcher_result->getRaw()));

      // allow modules to alter raw obj
      drupal_alter('feeds_socialmedia_fetcher_objs_' . $config['service'], $raw);

      foreach ($raw as $raw_obj) {

        $item = array();

        // allow modules to alter the element
        drupal_alter('feeds_socialmedia_fetcher_' . $config['service'], $item, $raw_obj, $mappings);

        $result->items[] = $item;
      }
    }

    return $result;
  }

  /**
  * Override parent::getMappingSources().
  */
  public function getMappingSources() {
    return array(
      'title' => array(
        'name' => t('Title'),
        'description' => t('Post title'),
      ),
      'content' => array(
        'name' => t('Content'),
        'description' => t('Post content'),
      ),
      'date' => array(
        'name' => t('Date'),
        'description' => t('Created at date for post'),
      ),
      'id' => array(
        'name' => t('ID'),
        'description' => t('Unique identifier'),
      ),
      'link' => array(
        'name' => t('Link'),
        'description' => t('External link to post'),
      ),
      'service' => array(
        'name' => t('Service'),
        'description' => t('Name of the socialmedia service (e.g. twitter)'),
      ),
      'icon_link' => array(
        'name' => t('Icon Link'),
        'description' => t('URL to user or page icon'),
      ),
    );
  }
}
